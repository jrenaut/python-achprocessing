from achprocessing import __version__
from setuptools import find_packages, setup

setup(
    name="python-achprocessing",
    version=__version__,
    packages=[
        "achprocessing",
    ],
    description="Python wrapper for achprocessing.com API",
    author="Jon Renaut",
    author_email="jon@coldants.com",
    url="https://gitlab.com/jrenaut/python-achprocessing",
    download_url="https://gitlab.com/jrenaut/python-achprocessing/-/archive/master/python-achprocessing-master.zip",
)
