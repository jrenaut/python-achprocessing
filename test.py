import pprint
from datetime import datetime, timedelta

from achprocessing.ach import Processor
from achprocessing.holders import *
from achprocessing.settings import API_KEY, BASE_URL, PASSWORD, USERNAME

pp = pprint.PrettyPrinter(depth=6)


def test_create():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    d = datetime.today()
    group = TransactionGroup(
        {
            "ActionType": "debit",
            "EffectiveDate": p.format_date(d),
            "EntryDescription": "Payment",
            "SecCode": "WEB",
            "UserData": "TEST USER DATA",
        }
    )
    group.add_transaction(
        {
            "AccountName": "Joe Jones",
            "AccountNumber": "34343434",
            "AccountType": "Checking",
            "Amount": "7.21",
            "RoutingNumber": "021502011",
            "UserData": "find_by_user_data_test",
            "IndividualIdentificationNumber": "TEST343434",
            "PaymentRelatedInformation": "Joe Jones Test Payment",
        }
    )
    # group.add_transaction({"AccountName" : "Mary Smith",
    #     "AccountNumber":"12121212",
    #     "AccountType":"Checking",
    #     "Amount" : "3.19",
    #     "RoutingNumber":"111111111",
    #     "UserData":"mary_smith_1234",
    #     "IndividualIdentificationNumber":"TEST121212",
    #     "PaymentRelatedInformation":"Mary Smith Test Payment"})
    resp = p.create_transaction(group)
    print(
        "Transaction was {0}".format("successful" if resp.success else "unsuccessful")
    )
    pp.pprint(resp.get())


def test_search():
    # print("credentials:{0} {1} {2}".format(API_KEY, USERNAME, PASSWORD))
    p = Processor(API_KEY, USERNAME, PASSWORD)
    so = SearchObject({"TransactionKeysList": "28385054,13581931"})
    # so = SearchObject(
    #     {
    #         "EffectiveDateStart": datetime.strptime("2019-07-1", "%Y-%m-%d"),
    #         "EffectiveDateEnd": datetime.strptime("2019-07-6", "%Y-%m-%d"),
    #     }
    # )
    resp = p.search(so)
    pp.pprint(resp.get())
    for res in resp.get_results():
        pp.pprint(res.get())


def test_create_by_API_KEY():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    p.create_transaction_by_API_KEY()


def test_create_recurring():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    recurring = RecurringItem(
        {
            "SecCode": "PPD",
            "ScheduleName": "RC test",
            "ScheduleDescription": "Test of RC",
            "AccountName": "Account Name",
            "AccountNumber": "6746681",
            "RoutingNumber": "031176110",
            "AccountType": "Saving",
            "ActionType": "Debit",
            "Frequency": "Monthly",
            "Interval": "1",
            "StartDate": "12/01/2019",
            "EntryDescription": "test A",
            "Amount": "1.00",
            "PaymentCount": "2",
            "User Data": "Some user data"
            # "TotalAmount":""
        }
    ).get()
    resp = p.create_recurring_transaction(recurring)
    print(
        "Transaction was {0}".format("successful" if resp.success else "unsuccessful")
    )
    pp.pprint(resp.get())


def test_list_recurring():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    resp = p.get_recurring_transactions()
    print(
        "Transaction was {0}".format("successful" if resp.success else "unsuccessful")
    )
    pp.pprint(resp.get())
    for item in resp.get_items():
        pp.pprint(item.get())


def test_get_recurring_by_id():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    # p.get_recurring('e50207a8-3e06-45bd-a9e2-b89cf67378ce')
    resp = p.get_recurring_transaction_by_id("47f78eea-e1b8-4e80-85bf-653b96b9a78c")
    print(
        "Transaction was {0}".format("successful" if resp.success else "unsuccessful")
    )
    pp.pprint(resp.get())
    for item in resp.get_payments():
        pp.pprint(item.get())


def test_hold_and_release():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    hold = p.hold_recurring("47f78eea-e1b8-4e80-85bf-653b96b9a78c")
    pp.pprint(hold.get())
    release = p.release_recurring("47f78eea-e1b8-4e80-85bf-653b96b9a78c")
    pp.pprint(release.get())


def test_account():
    a = TransactionObject(
        {
            "AccountName": "Test User",
            "AccountNumber": "465785469",
            "AccountType": "Checking",
            "Amount": "59.17",
            "BadValue": "elephant",
            "RoutingNumber": "021502011",
            "UserData": "CA12345",
            "IndividualIdentificationNumber": "TEST1234567",
            "PaymentRelatedInformation": "TestPayment",
        }
    )
    print(a.get())


def test_stop_payment():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    resp = p.stop_recurring("47f78eea-e1b8-4e80-85bf-653b96b9a78c")  # schedule id
    # resp = p.stop_recurring("e0fcfcf7-aeac-4998-9192-7159bcffbdd7")  # payment id
    pp.pprint(resp.get())


def test_date():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    d = datetime.today()
    print(p.format_date(d))


def test_cred_min_date():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    resp = p.get_credit_minimum_date()
    pp.pprint(resp.get())


def test_get_returns():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    d = datetime.today()
    resp = p.get_returns(p.format_date(d - timedelta(days=30)), p.format_date(d))


def test_reconcile():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    d = datetime.today()
    print("-------------GET BY ID-------------------")
    resp = p.reconcile_transaction_by_id("30128832")
    print("-------------RESPONSE----------------------")
    print(resp.get())
    print("-------------GET BY USER DATA-------------------")
    resp = p.reconcile_transaction_by_userdata("OI5N1HSD")
    print("-------------RESPONSE----------------------")
    print(resp.get())


def do_client_test():
    p = Processor(API_KEY, USERNAME, PASSWORD)
    # cred_min_date = p.get_credit_minimum_date()
    # resp = p.reconcile_transaction_by_id('13581843')
    resp = p.reconcile_transaction_by_userdata("TEST USER DATA")
    print(resp.get())
    if False:
        group = TransactionGroup(
            {
                "ActionType": "debit",
                "EffectiveDate": p.format_date(cred_min_date.get_date()),
                "EntryDescription": "Payment",
                "SecCode": "WEB",
                "UserData": "TEST USER DATA",
            }
        )
        group.add_transaction(
            {
                "AccountName": "Herb Jones",
                "AccountNumber": "11112124",
                "AccountType": "Checking",
                "Amount": "12.18",
                "RoutingNumber": "a12b3c4d",
                "UserData": "herb_jones_9734068",
                "IndividualIdentificationNumber": "TEST141325",
                "PaymentRelatedInformation": "Herb Jones Test Payment",
            }
        )
        print(group.get())
        resp = p.create_transaction(group)
    if False:
        group = TransactionGroup(
            {
                "ActionType": "credit",
                "EffectiveDate": p.format_date(
                    cred_min_date.get_date() + timedelta(days=3)
                ),
                "EntryDescription": "Payment",
                "SecCode": "WEB",
                "UserData": "TEST USER DATA",
            }
        )
        group.add_transaction(
            {
                "AccountName": "Mary Smith",
                "AccountNumber": "12121213",
                "AccountType": "Checking",
                "Amount": "2.19",
                "RoutingNumber": "021502011",
                "UserData": "mary_smith_9223",
                "IndividualIdentificationNumber": "TEST5698",
                "PaymentRelatedInformation": "Mary Smith Test Payment",
            }
        )
        group.add_transaction(
            {
                "AccountName": "Jill Smith",
                "AccountNumber": "12121214",
                "AccountType": "Checking",
                "Amount": "4.19",
                "RoutingNumber": "021502011",
                "UserData": "jill_smith_92323",
                "IndividualIdentificationNumber": "TEST5668",
                "PaymentRelatedInformation": "Jill Smith Test Payment",
            }
        )
        print(group.get())
        resp = p.create_transaction(group)


if __name__ == "__main__":
    TEST = 12
    if TEST == 1:
        test_create()
    elif TEST == 2:
        test_search()
    elif TEST == 3:
        test_create_by_API_KEY()
    elif TEST == 4:
        test_create_recurring()
    elif TEST == 5:
        test_list_recurring()
    elif TEST == 6:
        test_get_recurring_by_id()
    elif TEST == 7:
        test_hold_and_release()
    elif TEST == 8:
        test_account()
    elif TEST == 9:
        test_date()
    elif TEST == 10:
        test_stop_payment()
    elif TEST == 11:
        test_cred_min_date()
    elif TEST == 12:
        test_get_returns()
    elif TEST == 13:
        test_reconcile()
    elif TEST == 14:
        do_client_test()
