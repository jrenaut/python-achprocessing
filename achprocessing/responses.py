from datetime import datetime


class AchResponse:
    def __init__(self, params):
        if params.get("Message", None):
            # This is likely an unhandled server error from the API
            raise Exception(params.get("Message"))

    def get(self, short=False):
        if short:
            if hasattr(self, "short_params"):
                return {
                    k: v
                    for k, v in iter(self.safe_params.items())
                    if k not in self.short_params
                }
        return self.safe_params

    @property
    def success(self):
        return self.safe_params.get("ErrorsOccurred", 1) == 0


class RecurringItemResponse(AchResponse):
    # keys to remove from response
    delete_keys = ["RoutingNumber", "SecCode"]
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]
    # Large lists and things that aren't necessarily needed every time
    short_params = ["PaymentList"]

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            acct_num = params.get("AccountNumber", None)
            if acct_num:
                # store last 4 of account number so users can identify the account
                params["AccountNumber"] = acct_num[-4:]
            for d in self.delete_keys:
                del params[d]
            self.safe_params = params


class TransactionGroupResponse(AchResponse):
    # Note this response doesn't contain bank info so nothing needs to be scrubbed
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred", "ErrorMessage"]

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {}
            self.safe_params["ErrorsOccurred"] = params.get("ErrorsOccurred", 1)
            self.safe_params["ErrorMsg"] = []
            for t in iter(params["TransactionGroupResponses"]):
                self.safe_params["ErrorMsg"].append(t["ErrorMessage"])
        else:
            self.safe_params = params


class ListItem(AchResponse):
    """
    Used to store recurring items returned by ach.get_recurring_transactions
    """

    # keys to remove from response
    delete_keys = ["RoutingNumber"]
    # remaining parameters that can safely be transmitted/stored
    safe_params = None

    def __init__(self, params):
        super().__init__(params)
        acct_num = params.get("AccountNumber", None)
        if acct_num:
            # store last 4 of account number so users can identify the account
            params["AccountNumber"] = acct_num[-4:]
        for d in self.delete_keys:
            del params[d]
        self.safe_params = params


class ListRecurringResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]
    recurring_items = []

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            for r in params["RecurringSchedule"]:
                self.recurring_items.append(ListItem(r))
            del params["RecurringSchedule"]
            self.safe_params = params

    def get_items(self):
        return self.recurring_items


class HoldReleaseResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            self.safe_params = params

    @property
    def is_active(self):
        return self.safe_params == "Active"

    @property
    def is_on_hold(self):
        return self.safe_params == "On Hold"


class RecurringPaymentResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]

    def __init__(self, params):
        super().__init__(params)
        self.safe_params = params


class Payment(AchResponse):
    error_params = ["ErrorMessage"]
    # remaining parameters that can safely be transmitted/stored
    safe_params = None

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorMessage", None) is not None:
            self.safe_params = {}
            self.safe_params["ErrorsOccurred"] = 1
            self.safe_params["ErrorMessage"] = params.get("ErrorMessage")
        else:
            self.safe_params = params


class RecurringScheduleResponse(AchResponse):
    # keys to remove from response
    delete_keys = ["RoutingNumber", "SecCode", "PaymentList"]
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]
    payments = []

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            acct_num = params.get("AccountNumber", None)
            if acct_num:
                # store last 4 of account number so users can identify the account
                params["AccountNumber"] = acct_num[-4:]
            for p in params["PaymentList"]:
                self.payments.append(Payment(p))
            for d in self.delete_keys:
                del params[d]
            self.safe_params = params

    def get_payments(self):
        return self.payments

    @property
    def active(self):
        return self.safe_params.get("Status", "Error") == "Active"


class StopRecurringResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            self.safe_params = params


class CreditMinimumDateResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            self.safe_params = params

    def get_date(self):
        if self.success:
            str_date = ""
            for a in self.safe_params.get("CreditMinimumDate").split("/"):
                if len(a) == 1:
                    str_date += "0" + a
                else:
                    str_date += a
            return datetime.strptime(str_date, "%m%d%Y")
        return None


class ReconcileTransactionResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]

    def __init__(self, params):
        super().__init__(params)
        self.safe_params = params


class SearchResult(AchResponse):
    error_params = ["ErrorMessage"]
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    delete_keys = ["RoutingNumber"]

    def __init__(self, params):
        super().__init__(params)
        if params.get("ErrorMessage", None) is not None:
            self.safe_params = {}
            self.safe_params["ErrorsOccurred"] = 1
            self.safe_params["ErrorMessage"] = params.get("ErrorMessage")
        else:
            acct_num = params.get("AccountNumber", None)
            if acct_num:
                # store last 4 of account number so users can identify the account
                params["AccountNumber"] = acct_num[-4:]
            for d in self.delete_keys:
                del params[d]
            self.safe_params = params


class SearchResponse(AchResponse):
    # remaining parameters that can safely be transmitted/stored
    safe_params = None
    # params containing error information
    error_params = ["ErrorMsg", "ErrorCode", "ErrorsOccurred"]
    delete_keys = ["EntryDetail", "AffectedRecords"]
    search_results = []

    def __init__(self, params):
        print(params)
        super().__init__(params)
        if params.get("ErrorsOccurred", 1) > 0:
            self.safe_params = {
                k: v for k, v in iter(params.items()) if k in self.error_params
            }
        else:
            records_returns = params.get("AffectedRecords", 0)
            params["ResultCount"] = int(records_returns)
            for p in params["EntryDetail"]:
                self.search_results.append(SearchResult(p))
            for d in self.delete_keys:
                del params[d]
            self.safe_params = params

    def get_results(self):
        """
        Returns list of SearchResponse objects
        """
        return self.search_results

    def result_count(self):
        """
        Returns result count as integer
        """
        return self.safe_params["ResultCount"]
