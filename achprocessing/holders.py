class Holder:
    def __init__(self, params={}):
        for k, v in iter(params.items()):
            if k in self.keys:
                self.kv[k] = v
            elif hasattr(self, "list_keys") and k in self.list_keys:
                self.kv[k] = [x.strip() for x in v.split(",")]
        for req in self.required:
            if req not in self.kv:
                raise Exception(
                    "Required key '{0}' not found in {1} initialization parameters".format(
                        req, self.__class__.__name__
                    )
                )

    def get(self):
        return self.kv


class SearchObject(Holder):
    def __init__(self, params={}):
        self.kv = {}
        super().__init__(params)
        if len(self.kv) == 0:
            raise Exception("Search requires at least one search parameter")
        for date_key in self.date_keys:
            if date_key in self.kv:
                # format date to this: 2020-12-30T12:00:00
                self.kv[date_key] = self.kv[date_key].strftime("%Y-%m-%d") + "T12:00:00"

    list_keys = ["TransactionKeysList", "ReturnTransactionKeys"]
    keys = [
        "AccountName",
        "AccountNumber",
        "Amount",
        "ApprovalDateEnd",
        "ApprovalDateStart",
        "EffectiveDateEnd",
        "EffectiveDateStart",
        "FileName",
        "IsApprovedTransaction",
        "LoadDateEnd",
        "LoadDateStart",
        "ACHProcessingStatus",
        "RemitDateEnd",
        "RemitDateStart",
        "RemitTransmissionDateEnd",
        "RemitTransmissionDateStart",
        "RoutingNumber",
        "SECCode",
        "TransmissionDateEnd",
        "TransmissionDateStart",
        "UserData",
        "VirtualFileName",
        "CheckSerialNumber",
        "IndividualIdentificationNumber",
        "PaymentRelatedInformation",
    ]
    date_keys = [
        "LoadDateEnd",
        "LoadDateStart",
        "ApprovalDateEnd",
        "ApprovalDateStart",
        "EffectiveDateEnd",
        "EffectiveDateStart",
        "RemitDateEnd",
        "RemitDateStart",
        "RemitTransmissionDateEnd",
        "RemitTransmissionDateStart",
        "TransmissionDateEnd",
        "TransmissionDateStart",
    ]
    required = []


class TransactionObject(Holder):
    def __init__(self, params={}):
        self.kv = {}
        super().__init__(params)

    keys = [
        "AccountName",
        "AccountNumber",
        "AccountType",
        "Amount",
        "RoutingNumber",
        "UserData",
        "IndividualIdentificationNumber",
        "PaymentRelatedInformation",
    ]
    required = [
        "AccountName",
        "AccountNumber",
        "AccountType",
        "Amount",
        "RoutingNumber",
    ]


class ApiTransactionObject(Holder):
    def __init__(self, params={}):
        self.kv = {}
        super().__init__(params)

    keys = [
        "AccountName",
        "ApiKey",
        "Amount",
        "UserData",
        "IndividualIdentificationNumber",
        "PaymentRelatedInformation",
    ]
    required = ["AccountName", "ApiKey", "Amount"]


class TransactionGroup(Holder):
    def __init__(self, params={}):
        self.kv = {}
        super().__init__(params)

    keys = [
        "ActionType",
        "EffectiveDate",
        "EntryDescription",
        "SecCode",
        "UserData",
        "TransactionObjects",
    ]
    required = ["ActionType", "EffectiveDate", "EntryDescription", "SecCode"]

    def add_transaction(self, params={}):
        if "TransactionObjects" in self.kv:
            self.kv["TransactionObjects"].append(TransactionObject(params).get())
        else:
            self.kv["TransactionObjects"] = [TransactionObject(params).get()]


class RecurringItem(Holder):
    def __init__(self, params={}):
        self.kv = {}
        super().__init__(params)
        self.extra_init_check()

    def extra_init_check(self):
        """Creating a recurring transaction requires any two of Amount, PaymentCount, and TotalAmount. The other value will be calculated"""
        counter = 0
        for k in ["Amount", "PaymentCount", "TotalAmount"]:
            if k in self.kv:
                counter += 1
        if counter < 2:
            raise Exception(
                "Two of 'Amount', 'PaymentCount', and 'TotalAmount' must be included in initialization parameters"
            )

    keys = [
        "SecCode",
        "ScheduleName",
        "ScheduleDescription",
        "AccountName",
        "AccountNumber",
        "RoutingNumber",
        "AccountType",
        "ActionType",
        "Frequency",
        "Interval",
        "StartDate",
        "EntryDescription",
        "Amount",
        "PaymentCount",
        "TotalAmount",
        "UserData",
    ]

    required = [
        "SecCode",
        "ScheduleName",
        "AccountName",
        "AccountNumber",
        "RoutingNumber",
        "AccountType",
        "ActionType",
        "Frequency",
        "Interval",
        "StartDate",
        "EntryDescription",
    ]
