import os

ENV = os.environ.get("ACHPROCESSING_ENV")
if ENV == "TEST":
    BASE_URL = "https://extest.achprocessing.com/Finanyzlrapi/api/{0}/{1}"
else:
    BASE_URL = "https://client.achprocessing.com/restapi/api/{0}/{1}"
API_KEY = os.environ.get("ACHPROCESSING_API_KEY")
USERNAME = os.environ.get("ACHPROCESSING_USERNAME")
PASSWORD = os.environ.get("ACHPROCESSING_PASSWORD")
