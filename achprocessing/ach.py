import logging
import pprint

import requests
from achprocessing.responses import *
from achprocessing.settings import BASE_URL

logger = logging.getLogger(__name__)


class Processor:
    def __init__(self, apikey, username, password):
        self.apikey = apikey
        self.username = username
        self.password = password

    def _get_auth(self):
        return {
            "ApiKey": self.apikey,
            "UserName": self.username,
            "Password": self.password,
        }

    def create_transaction(self, trans_group):
        url = BASE_URL.format(
            "ach",
            "CreateTransaction",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["TransactionGroups"] = [trans_group.get()]
        # print(payload)
        r = requests.post(url, json=payload)
        return TransactionGroupResponse(r.json())

    def create_sameday_transaction(self, trans_group):
        url = BASE_URL.format(
            "ach",
            "CreateSameDayTransaction",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["TransactionGroups"] = [trans_group.get()]
        # print(payload)
        r = requests.post(url, json=payload)
        return TransactionGroupResponse(r.json())

    def create_recurring_transaction(self, recurring, user_data=None):
        url = BASE_URL.format(
            "recurring",
            "CreateSchedule",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["RecurringScheduleRequestObject"] = recurring
        if user_data:
            payload["userData"] = user_data
        logger.debug(url)
        logger.debug(payload)
        r = requests.post(url, json=payload)
        logger.debug(r.json())
        return RecurringItemResponse(r.json())

    def get_recurring_transactions(self, status="Active", order="1"):
        url = BASE_URL.format(
            "recurring",
            "GetScheduleList",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["Status"] = status
        payload["Order"] = order
        logger.debug(url)
        logger.debug(payload)
        r = requests.post(url, json=payload)
        return ListRecurringResponse(r.json())

    def _recurring_by_id(self, id, url):
        url = BASE_URL.format(
            "recurring",
            url,
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["ScheduleId"] = id
        r = requests.post(url, json=payload)
        return r.json()

    def hold_recurring(self, id):
        return HoldReleaseResponse(self._recurring_by_id(id, "HoldSchedule"))

    def release_recurring(self, id):
        return HoldReleaseResponse(self._recurring_by_id(id, "ReleaseSchedule"))

    def get_recurring(self, payment_id):
        return ReccuringPaymentResponse(self._recurring_by_id(payment_id, "GetPayment"))

    def stop_recurring(self, id):
        return StopRecurringResponse(self._recurring_by_id(id, "StopSchedule"))

    def get_recurring_transaction_by_id(self, schedule_id):
        return RecurringScheduleResponse(
            self._recurring_by_id(schedule_id, "GetScheduleByScheduleId")
        )

    def get_credit_minimum_date(self):
        url = BASE_URL.format(
            "ach",
            "CreditMinimumDate",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        r = requests.post(url, json=payload)
        # print(payload)
        return CreditMinimumDateResponse(r.json())

    def _reconcile_transaction(self, transaction_id, user_data):
        url = BASE_URL.format(
            "ach",
            "ReconcileTransaction",
        )
        payload = {}
        search = {}
        payload["Authentication"] = self._get_auth()
        if transaction_id:
            payload["transactionEntryDetailUkey"] = transaction_id
        if user_data:
            url = BASE_URL.format("ach", "Search")
            search["UserData"] = user_data
            payload["search"] = search
        # print(payload)
        r = requests.post(url, json=payload)
        return ReconcileTransactionResponse(r.json())

    def reconcile_transaction_by_id(self, id):
        return self._reconcile_transaction(id, None)

    def reconcile_transaction_by_userdata(self, user_data):
        return self._reconcile_transaction(None, user_data)

    def get_returns(self, start_date, end_date, user_data=None):
        url = BASE_URL.format(
            "ach",
            "GetReturns",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["StartDate"] = start_date
        payload["EndDate"] = end_date
        if user_data:
            payload["UserData"] = user_data
        # print(payload)
        r = requests.post(url, json=payload)
        # print(r.json())

    def create_transaction_by_apikey(self):
        url = BASE_URL.format(
            "ach",
            "CreateTransactionApiKey",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        trans_objs = []
        trans_objs.append(
            {
                "AccountName": "Test User",
                "ApiKey": None,
                "Amount": "59.17",
                "UserData": "CA12345",
                "IndividualIdentificationNumber": "TEST1234567",
                "PaymentRelatedInformation": "TestPayment",
            }
        )
        payload["TransactionGroups"] = [
            {
                "ActionType": "debit",
                "EffectiveDate": "2020-10-06T12:00:00",
                "EntryDescription": "Payment",
                "SecCode": "WEB",
                "UserData": "CA54321",
                "TransactionObjects": trans_objs,
            }
        ]
        r = requests.post(url, json=payload)
        # pp = pprint.PrettyPrinter(depth=6)
        # pp.pprint(payload)
        # pp.pprint(r.json())

    def search(self, so):
        """
        Required argument: achprocessing.holders.SearchObject
        """
        url = BASE_URL.format(
            "ach",
            "Search",
        )
        payload = {}
        payload["Authentication"] = self._get_auth()
        payload["search"] = so.get()
        # payload['search'] = {"LoadDateStart":"2018-08-27T12:00:00", "LoadDateEnd":"2018-08-31T12:00:00"}
        # payload['search'] = {"TransactionKeysList": [13581857]}
        # payload['search'] = {'Amount':'1.00'}
        # payload['search'] = {'SECCode':'WEB'}
        # payload['search'] = {'RoutingNumber':'021502011'}
        # print(payload)
        r = requests.post(url, json=payload)
        # pp = pprint.PrettyPrinter(depth=6)
        # print(r.json())
        return SearchResponse(r.json())

    def format_date(self, dt):
        if isinstance(dt, str):
            d = datetime.strptime(dt, "%Y-%m-%d")
        else:
            d = dt
        return d.strftime("%Y-%m-%dT%H:%M:%S")
